let data;
function loaded() {
    fetch('https://api.themoviedb.org/3/movie/popular?api_key=de95a317ffd5c7d771e43cf0fb7c1bc5&language=en-US&page=1').then(function (res) {
        // console.log(res.json());
        return res.json();
    }).then(function (res) {
        console.log(res);
        data = res;
        paintTheData(data, null);
    })
}

function filterClick(vote_average) {
    paintTheData(data, vote_average);
}

function paintTheData(vote_data, vote_average) {

    let ulElement = document.getElementById("block");
    // console.log(ulElement.innerHTML);
    ulElement.innerHTML = "";
    if (vote_average != null) {
        if (vote_average == 6) {
            vote_data.results = vote_data.results.filter((abc) => {
                return abc.vote_average >=
                    vote_average;
            })
        } else {
            vote_data.results = vote_data.results.filter((abc) => {
                return abc.vote_average > 6;
            });
        }
    }
    for (let i = 10; i < vote_data.results.length; i++) {


        ulElement.innerHTML += '<div class="divBlock"><span class="a">\
            <img src="https://image.tmdb.org/t/p/w185_and_h278_bestv2' + vote_data.results[i].poster_path + '"></span>\
            <span>\
                 <p>\
                    <h4>' + vote_data.results[i].title + '</h4>\
                    <p>May 25 2018</p>\
                    <p>' + vote_data.results[i].overview.substr(0, 200) + '</p>\
                    <br>\
                    <p class="d">More info</p>\
                </p>\
            </span></div>';
    }
}