function bodyLoaded() {
    "use strict"
    v=10;
    console.log(v);
    let x=1;
    var y=1;{
        let x=2;//let has a block scope,i.e it is available only in the block that it's written type
        var y=2; // var has local scope i.e it is available only in the function it's written type
        const z=3;// const has a block scope,i.e it is available only in the block that it's written type and
                  //you con't change it's value once it is defined.
        console.log(x);//2
        console.log(y);//2
        console.log(x);
        console.log(y);

    }
    if(1){
        let p = 2;
        var c = 3;
    }
    //console.log(p);
    console.log(c);
    console.log(x);//1
    console.log(y);//2

}
//console.log(c);


//print 5 if it's 4.4 and print 4 if it is 4.8 without using if else.
//loop obj keys using for loop
//while and do-while //give me atleast one example of while & do-while &switch
//find if given variable is an array or not
//show date in 23 june 2018 10:10:10 pm format.