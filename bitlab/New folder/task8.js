function loaded(){
	let ulElement = document.getElementById("divId");
	console.log(ulElement);
	fetch(' https://stumped.herokuapp.com/api/posts').then(function(res){
		return res.json();
	}).then(function (result){
		console.log(result);

		for(let i=0; i<result.objects.length;i++){
			let li=document.createElement('li');
			let img=document.createElement('img');
            let span=document.createAttribute('span');
            let p =document.createElement('p');
            let hr =document.createElement('hr');
			img.src=result.objects[i].employer.profile_image.image_url;
			img.className ="class";
            span.innerHTML = result.objects[i].employer.company_name;
            p.innerHTML = result.objects[i].employer.instahyre_note;
			li.appendChild(img);
            li.appendChild(span);
            li.appendChild(p);
            li.appendChild(hr);
			ulElement.appendChild(li);
		}
	}).catch(function(err){
		console.log(err);
	});
	console.log("new page");
}